package estate;

public class outdoor extends home {
    private String name;


    public outdoor(String type, String name, int price, String forWhat) {
        super(type, name, price, forWhat);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    }
