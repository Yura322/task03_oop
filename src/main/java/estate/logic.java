package estate;

import java.util.ArrayList;
import java.util.List;

public class logic extends garden implements Comparable<chemistry> {


    public logic(String type, String name, int price, String forWhat) {
        super(type, name, price, forWhat);
    }

    public int compareTo(chemistry o) {
        if (this.getPrice() == o.getPrice()) {
            return 0;
        }
        if (this.getPrice() < o.getPrice()) {
            return -1;
        } else {
            return 1;
        }

    }
}

